module Main where

import System.IO(stderr, hPrint)
import System.Environment(getArgs, getProgName)
import System.Exit(exitFailure, exitSuccess)

import Parser(parse)
import ErrM(Err(Bad, Ok))
import TypeCheck(TypeError, typeCheck)
import Interpreter(eval, RuntimeError)

main :: IO ()
main = do
    args <- getArgs
    case args of
        [f] -> do
            cont <- readFile f
            case parse cont of
                Bad s -> do
                    hPrint stderr s
                    exitFailure
                Ok prog ->
                    case typeCheck prog of
                        Left e -> do
                            hPrint stderr e
                            exitFailure
                        _ -> do
                            me <- eval prog
                            case me of
                                 Left e -> do
                                     hPrint stderr e
                                     exitFailure
                                 Right () -> exitSuccess
        _ -> do
            name <- getProgName
            putStrLn $ "Usage: ./" ++ name ++ " <source>"
