module TypeCheck where
import Abssyntax
import Printsyntax(printTree)

import Control.Monad.Trans.Except(ExceptT, runExceptT)
import Control.Monad.Identity(Identity, runIdentity)
import Control.Monad.Trans.Reader(ReaderT, runReaderT, ask, local)
import Control.Monad.Error.Class(throwError)
import Control.Monad.State(get, put, StateT, runStateT)
import Control.Monad(when, unless, forM_, void)
import Text.Format(format)
import Data.List(intercalate)
import qualified Data.Map.Strict as Map

data TypeError = TE ErrorKind Stm

instance Show (TypeError) where
    show (TE e stm) = format "{0}\nFound at:\n {1}" [show e, printTree stm]

data ErrorKind =
    NoVariable Ident |
    BadType Type Type |
    CmpOpError Type Type |
    EqOpError Type Type |
    NotArray Type |
    ArrayAssignError |
    NotLvalue Exp |
    CallingNotFunction Type |
    WrongArgsNum Int Int |
    NotPrintable Type |
    AssignFun Ident

instance Show (ErrorKind) where
    show (NoVariable (Ident n)) = format "There is no {0} variable" [n]
    show (BadType t1 t2) = format "Wrong type - {1}, {0} expected" [show t1,
                                                                    show t2]
    show (CmpOpError t1 t2) = format ("Only integers are ordered, " ++
                                     "trying to compare {0} with {1}") [show t1,
                                                                        show t2]
    show (EqOpError t1 t2) = format "Cannot compare {0} with {1}" [show t1,
                                                                   show t2]
    show (NotArray t) = format "Type {0} is not an array type" [show t]
    show (ArrayAssignError) = format "Cannot assign to array" []
    show (NotLvalue e) = format "Expression {0} is not l-value" [printTree e]
    show (CallingNotFunction t) = format "Type {0} is not a function" [show t]
    show (WrongArgsNum gi i) = format ("Calling function with {0} arguments" ++
                                       " but {1} required") [show gi, show i]
    show (NotPrintable t) = format "Type {0} is not printable" [show t]
    show (AssignFun (Ident n)) =
        format "'{0}' is a function. Can't assign to it" [n]

data BasicType = BInt | BBool | BF BasicType [ArgType] deriving (Eq)
data ArrType = TArr Type deriving (Eq)
data Type = TB BasicType | TA ArrType deriving (Eq)
data ArgType = ArgVal Type | ArgRef Type deriving (Eq)

instance Show BasicType where
    show BInt = "int"
    show BBool = "bool"
    show (BF bt at) = format "<{0}({1})>" [show bt,
                                           intercalate "," (map show at)]
instance Show ArrType where
    show (TArr t) = format "{0}[]" [show t]

instance Show Type where
    show (TB tb) = show tb
    show (TA ta) = show ta

instance Show ArgType where
    show (ArgVal t) = show t
    show (ArgRef (TB tb)) = format "{0}&" [show tb]
    show (ArgRef (TA ta)) = show ta

{- the second argument it true iff variable is assignable -}
type EnvVal = (Type, Bool)

type Env = Map.Map Ident EnvVal

type TypeEval a = ReaderT Env (ExceptT TypeError (StateT Stm Identity)) a

updateMap :: Ord k => [(k, v)] -> Map.Map k v -> Map.Map k v
updateMap l m = foldl (\ a (k, v) -> Map.insert k v a) m l

runTypeCheck :: TypeEval a -> Either TypeError a
runTypeCheck m =
    fst $ runIdentity (runStateT (runExceptT (runReaderT m Map.empty))
                      (SBlock []))

typeCheck :: Program -> Either TypeError ()
typeCheck = runTypeCheck . checkProgram

checkProgram :: Program -> TypeEval ()
checkProgram (Prog decls) = checkDecls decls

checkDecls :: [SDecl] -> TypeEval ()
checkDecls (d:ds) = do
    put (SDec d)
    (n, t) <- checkDecl d
    local (Map.insert n t) (checkDecls ds)

checkDecls [] = return ()

checkDecl :: SDecl -> TypeEval (Ident, EnvVal)
checkDecl (SFunDecl (Fun r n args stms)) = do
    (bt@(BF ret _), cleanArgs) <- checkFun (r, args, stms)
    let fun = (n, (TB bt, False))
    local (updateMap (fun:cleanArgs)) (checkStms stms ret)
    return fun

checkDecl (SBDecl (BasicDec bt n)) = return (n, (TB (getBasicType bt), True))

checkDecl (SBDecl (BasicDecAss bt n e)) = do
    let t = TB $ getBasicType bt
    checkExpType e t
    return (n, (t, True))

checkDecl (SBDecl (ArrayDec d n)) = do
    t <- checkArray d
    return (n, (t, True))

checkFun :: (BType, [ArgDecl], [Stm]) ->
    TypeEval (BasicType, [(Ident, EnvVal)])
checkFun (r, args, _) = do
    args' <- checkArgs args
    let ret = getBasicType r
    let cleanArgs = map (\(n, t) -> (n, (removeRef t, True))) args'
    return (BF ret (map snd args'), cleanArgs)

getBasicType :: BType -> BasicType
getBasicType TInt = BInt
getBasicType TBool = BBool
getBasicType (TFun r args) = BF (getBasicType r) (map getArgType args)

removeRef :: ArgType -> Type
removeRef (ArgVal t) = t
removeRef (ArgRef t) = t

checkArgs :: [ArgDecl] -> TypeEval [(Ident, ArgType)]
checkArgs argDecls =
    return $ map (\(ArgDec decl n) -> (n, getArgType decl)) argDecls

getArgType :: ArgDeclarator -> ArgType
getArgType (RefDeclar (BArgDeclar t)) = ArgRef (TB $ getBasicType t)
getArgType (LiftBArgDeclar (BArgDeclar t)) = ArgVal (TB $ getBasicType t)
getArgType (LiftArrayArgDeclar decl) = let
    getArrType (ArrayArgDeclar d) = TA $ TArr (getArrType d)
    getArrType (ArrayArgDeclarEnd (BArgDeclar t)) =
        TA $ TArr (TB $ getBasicType t)
  in ArgRef $ getArrType decl

checkStms :: [Stm] -> BasicType -> TypeEval ()
checkStms (stm@(SDec decl):stms) ret = do
    put stm
    (n, t) <- checkDecl decl
    local (Map.insert n t) (checkStms stms ret)

checkStms (stm:stms) ret = do
    put stm
    checkStm stm ret
    checkStms stms ret

checkStms [] _ = return ()

checkStm :: Stm -> BasicType -> TypeEval ()
checkStm (SWhile e stm) ret = do
    checkExpType e bool
    checkStms [stm] ret

checkStm (SIfElse e stm1 stm2) ret = do
    checkExpType e bool
    checkStms [stm1, stm2] ret

checkStm (SIf e stm) ret = do
    checkExpType e bool
    checkStms [stm] ret

checkStm (SExp e) _ = void $ checkExp e

checkStm (SBlock stms) ret = checkStms stms ret

checkStm (SReturn e) ret = void $ checkExpType e (TB ret)

checkStm (stm@(SDec _)) ret = checkStms [stm] ret

checkStm (SPrint e) _ = do
    t <- checkExp e
    when (t /= bool && t /= int) (throwTypeError $ NotPrintable t)

checkStm (SFor e1 e2 e3 stm) ret = do
    forM_ [e1, e3] checkExp
    checkExpType e2 bool
    checkStms [stm] ret

checkExp :: Exp -> TypeEval Type
checkExp (ELambda args r stms) = do
    (fun@(BF ret _), cleanArgs) <- checkFun (r, args, stms)
    local (updateMap cleanArgs) (checkStms stms ret)
    return $ TB fun

checkExp (EConst (ConstBool _)) = return bool
checkExp (EConst (ConstInteger _)) = return int

checkExp (EVar n) = do
    env <- ask
    case Map.lookup n env of
        Nothing -> throwTypeError (NoVariable n)
        Just (t, _) -> return t

checkExp (EOr e1 e2) = checkBinBoolOp e1 e2
checkExp (EAnd e1 e2) = checkBinBoolOp e1 e2

checkExp (EArithm1 e1 _ e2) = checkBinArithmOp e1 e2
checkExp (EArithm2 e1 _ e2) = checkBinArithmOp e1 e2

checkExp (EPostIncr e) = checkPrePostOp e
checkExp (EPostDecr e) = checkPrePostOp e
checkExp (EPreIncr e) = checkPrePostOp e
checkExp (EPreDecr e) = checkPrePostOp e

checkExp (EUnary UnaryNeg e) = checkExpType e bool
checkExp (EUnary _ e) = checkExpType e int

checkExp (EEq e1 e2) = checkEqOp e1 e2
checkExp (ENeq e1 e2) = checkEqOp e1 e2
checkExp (EComp e1 _ e2) = do
    t1 <- checkExp e1
    t2 <- checkExp e2
    unless (t1 == t2 && satisfiesCmp t1) (throwTypeError $ CmpOpError t1 t2)
    return bool

checkExp (EArray ea ei) = do
    ta <- checkExp ea
    checkExpType ei int
    case ta of
        TA (TArr t) -> return t
        _ -> throwTypeError $ NotArray ta

checkExp (EAss e1 Assign e2) = do
    checkLvalue e1
    t1 <- checkExp e1
    t2 <- checkExp e2
    case t1 of
        TA _ -> throwTypeError ArrayAssignError
        TB _ -> do
            unless (t1 == t2) (throwTypeError $ BadType t1 t2)
            return t1

checkExp (EAss e1 _ e2) = do
    checkLvalue e1
    checkBinArithmOp e1 e2

checkExp (ECall e es) = do
    ft <- checkExp e
    case ft of
        TB (BF ret args) -> do
            let (gl, l) = (length args, length es)
            unless (gl == l) (throwTypeError $ WrongArgsNum gl l)
            forM_ (zip args es) checkArgMatch
            return $ TB ret
        _ -> throwTypeError $ CallingNotFunction ft

checkArgMatch :: (ArgType, Exp) -> TypeEval ()
checkArgMatch (ArgVal t, e) = void $ checkExpType e t
checkArgMatch (ArgRef t, e) = do
    checkLvalue e
    void $ checkExpType e t

checkPrePostOp :: Exp -> TypeEval Type
checkPrePostOp e = do
    checkLvalue e
    checkExpType e int

checkLvalue :: Exp -> TypeEval ()
checkLvalue (EVar n) = do
    env <- ask
    case Map.lookup n env of
        Just (_, False) -> throwTypeError (AssignFun n)
        _ -> return ()

checkLvalue (EPreDecr e) = checkLvalue e
checkLvalue (EPreIncr e) = checkLvalue e
checkLvalue (EAss e _ _) = checkLvalue e
checkLvalue (EArray _ _) = return ()
checkLvalue e = throwTypeError $ NotLvalue e

checkExpType :: Exp -> Type -> TypeEval Type
checkExpType e t = do
    t' <- checkExp e
    unless (t == t') (throwTypeError $ BadType t t')
    return t

checkBinBoolOp :: Exp -> Exp -> TypeEval Type
checkBinBoolOp e1 e2 = do
    checkExpType e1 bool
    checkExpType e2 bool

checkBinArithmOp :: Exp -> Exp -> TypeEval Type
checkBinArithmOp e1 e2 = do
    checkExpType e1 int
    checkExpType e2 int

checkEqOp :: Exp -> Exp -> TypeEval Type
checkEqOp e1 e2 = do
    t1 <- checkExp e1
    t2 <- checkExp e2
    unless (t1 == t2 && satisfiesEq t1) (throwTypeError $ EqOpError t1 t2)
    return bool

satisfiesEq :: Type -> Bool
satisfiesEq (TB BInt) = True
satisfiesEq (TB BBool) = True
satisfiesEq _ = False

satisfiesCmp :: Type -> Bool
satisfiesCmp (TB BInt) = True
satisfiesCmp _ = False

bool :: Type
bool = TB BBool
int :: Type
int = TB BInt

checkArray :: ArrDeclarator -> TypeEval Type
checkArray arrDecl = let
    checkArrayRec (ArrayDeclar d e) = do
        checkExpType e int
        at <- checkArrayRec d
        return $ TA (TArr at)
    checkArrayRec (ArrayDeclarEnd bt e) = do
        checkExpType e int
        return $ TA (TArr (TB $ getBasicType bt))
  in checkArrayRec arrDecl

throwTypeError :: ErrorKind -> TypeEval a
throwTypeError err = do
    stm <- get
    throwError $ TE err stm
