module Interpreter where
import Abssyntax
import Printsyntax(printTree)

import qualified Data.Map.Strict as Map
import qualified Data.Array as Array
import Control.Monad.Trans.Except(ExceptT, runExceptT)
import Control.Monad.Trans.Reader(ReaderT, runReaderT)
import Control.Monad.Trans.Either(EitherT, runEitherT, left, right)
import Control.Monad.State(get, put, StateT, runStateT)
import Control.Monad.Error.Class(throwError)
import Control.Monad.Trans.Class(lift)
import Control.Monad.IO.Class(liftIO)
import Control.Monad.Reader.Class(ask, local, asks)
import Control.Monad(when, forM, void)
import Data.List(genericReplicate)

import Text.Format(format)

data ArgType = ArgRef | ArgVal

type MInt = Integer
type MBool = Bool
data Type = TI | TB | TO

{- F args stms env mn ret - mn is for the name of function; necessary for
-  recursion -}
data Fun = F [(Ident, ArgType)] [Stm] Env (Maybe Ident) Type
type Loc = Int
{- Not anonymous functions are kept directly in the environment -}
data EnvVal = EVL Loc | EVF Fun
type Env = Map.Map Ident EnvVal
{- State = (map, l, stm) - stm is the last instruction (info for errors),
- l is the first unused location -}
type State = (Map.Map Loc Value, Loc, Stm)
{- VU is for undefined value -}
data Value = VI MInt | VB MBool | VF Fun | VA (Array.Array MInt Loc) | VU

data RuntimeError = RE ErrorKind Stm

instance Show RuntimeError where
    show (RE NoMainFun _) = show NoMainFun
    show (RE e stm) = format "{0}\nFound at:\n {1}" [show e, printTree stm]

data ErrorKind =
    IndexOutOfBounds Exp MInt |
    NoMainFun |
    ZeroDivision Exp |
    NotPositiveArraySize Exp |
    BadCall Exp

instance Show ErrorKind where
    show (IndexOutOfBounds e i) =
        format "Index out of bounds. '{0}' not within [0, {1})" [printTree e,
                                                                 show i]
    show NoMainFun = format "There is no main function" []
    show (ZeroDivision e) =
        format "Division by '{0}' which equals 0" [printTree e]
    show (NotPositiveArraySize e) =
        format "Array size '{0}' is not positive" [printTree e]
    show (BadCall e) = format "Cannot call '{0}'" [printTree e]

type Ev = ReaderT Env (ExceptT RuntimeError (StateT State IO))
type Eval a =  Ev a
type FunEval a b = EitherT a Ev b

getType :: BType -> Type
getType TBool = TB
getType TInt = TI
getType _ = TO

runFun :: FunEval a () -> Eval (Either a ())
runFun = runEitherT

run :: Eval () -> IO (Either RuntimeError ())
run m = do
    (res, _) <- runStateT (runExceptT (runReaderT m Map.empty))
                          (Map.empty, 0, SBlock [])
    return res

eval :: Program -> IO (Either RuntimeError ())
eval = run . evalProgram

evalProgram :: Program -> Eval ()
evalProgram (Prog decls) = do
    env <- evalDecls decls
    case Map.lookup (Ident "main") env of
        Just (EVF fun) -> void $ callFun fun []
        _ -> throwRuntimeError NoMainFun

evalDecls :: [SDecl] -> Eval Env
evalDecls (d:ds) = do
    putStm (SDec d)
    env <- evalDecl d
    local (const env) (evalDecls ds)

evalDecls [] = ask

evalDecl :: SDecl -> Eval Env
evalDecl (SFunDecl (Fun r n args stms)) = do
    env <- ask
    let fun = F (evalArgs args) stms env (Just n) (getType r)
    return $ Map.insert n (EVF fun) env

evalDecl (SBDecl (BasicDec bt n)) = do
    l <- newLoc (getType bt)
    env <- ask
    return $ Map.insert n (EVL l) env

evalDecl (SBDecl (BasicDecAss bt n e)) = do
    l <- newLoc (getType bt)
    env <- ask
    v <- evalExp e
    assign l v
    return $ Map.insert n (EVL l) env

evalDecl (SBDecl (ArrayDec arrDecl n)) = do
    env <- ask
    l <- allocArray arrDecl
    return $ Map.insert n (EVL l) env

evalArgs :: [ArgDecl] -> [(Ident, ArgType)]
evalArgs decls = let
    evalArg (ArgDec (LiftArrayArgDeclar _) n) = (n, ArgRef)
    evalArg (ArgDec (LiftBArgDeclar _) n) = (n, ArgVal)
    evalArg (ArgDec (RefDeclar _) n) = (n, ArgRef)
  in foldr (\decl a -> evalArg decl:a) [] decls

getDefVal :: Type -> Value
getDefVal TI = VI 0
getDefVal TB = VB False
getDefVal TO = VU

newLoc :: Type -> Eval Loc
newLoc t = do
    (st, ml, stm) <- get
    put (Map.insert ml (getDefVal t) st, ml + 1, stm)
    return ml

evalExp :: Exp -> Eval Value
evalExp (EPreIncr e) = evalPrePostOp e True (1 +)
evalExp (EPreDecr e) = evalPrePostOp e True (\x -> x - 1)
evalExp (EPostIncr e) = evalPrePostOp e False (1 +)
evalExp (EPostDecr e) = evalPrePostOp e False (\x -> x - 1)

evalExp (EConst c) =
    case c of
        ConstInteger i -> return (VI i)
        ConstBool BooleanTrue -> return (VB True)
        ConstBool BooleanFalse -> return (VB False)

evalExp (EVar n) = do
    Just ev <- asks (Map.lookup n)
    case ev of
        EVL l -> getVal l
        EVF f -> return $ VF f

evalExp (ELambda args ret stms) = do
    let args' = evalArgs args
    env <- ask
    return $ VF (F args' stms env Nothing (getType ret))

evalExp (ECall e es) = do
    v <- evalExp e
    case v of
        VF f -> callFun f es
        VU -> throwRuntimeError $ BadCall e

evalExp (e@(EArray{})) = do
    l <- getAddr e
    getVal l

evalExp (EUnary UnaryPlus e) = evalExp e

evalExp (EUnary UnaryMinus e) = do
    VI i <- evalExp e
    return $ VI (-i)

evalExp (EUnary UnaryNeg e) = do
    VB b <- evalExp e
    return $ VB (not b)

evalExp (EArithm1 e1 BinPlus e2) = evalBinArithmOp e1 e2 (+)
evalExp (EArithm1 e1 BinMinus e2) = evalBinArithmOp e1 e2 (-)
evalExp (EArithm2 e1 BinMul e2) = evalBinArithmOp e1 e2 (*)
evalExp (EArithm2 e1 BinDiv e2) = do
    VI i <- evalExp e2
    when (i == 0) (throwRuntimeError $ ZeroDivision e2)
    evalBinArithmOp e1 (EConst $ ConstInteger i) div

evalExp (EOr e1 e2) = do
    VB b <- evalExp e1
    if b then return $ VB b else evalExp e2

evalExp (EAnd e1 e2) = do
    VB b <- evalExp e1
    if not b then return $ VB b else evalExp e2

evalExp (EEq e1 e2) = evalBinCmpOp e1 e2 liftEq
evalExp (ENeq e1 e2) = evalBinCmpOp e1 e2 (\a b -> not $ liftEq a b)

evalExp (EComp e1 op e2) = let
    getOp CompLt = (<)
    getOp CompLte = (<=)
    getOp CompGt = (>)
    getOp CompGte = (>=)
  in evalBinCmpOp e1 e2 (liftCmpOp (getOp op))

evalExp (e@(EAss{})) = do
    l <- getAddr e
    getVal l

liftCmpOp :: (MInt -> MInt -> Bool) -> Value -> Value -> Bool
liftCmpOp f (VI i1) (VI i2) = f i1 i2

liftEq :: Value -> Value -> Bool
liftEq (VI i1) (VI i2) = i1 == i2
liftEq (VB b1) (VB b2) = b1 == b2

evalBinCmpOp :: Exp -> Exp -> (Value -> Value -> Bool) -> Eval Value
evalBinCmpOp e1 e2 f = do
    v1 <- evalExp e1
    v2 <- evalExp e2
    return $ VB (f v1 v2)

evalBinArithmOp :: Exp -> Exp -> (MInt -> MInt -> MInt) -> Eval Value
evalBinArithmOp e1 e2 f = do
    VI i1 <- evalExp e1
    VI i2 <- evalExp e2
    return $ VI (f i1 i2)

evalPrePostOp :: Exp -> Bool -> (MInt -> MInt) -> Eval Value
evalPrePostOp e b f = do
    l <- getAddr e
    ov@(VI i) <- getVal l
    let nv = VI $ f i
    assign l nv
    if b then return nv else return ov

allocArray :: ArrDeclarator -> Eval Loc
allocArray decl = let
    getIndexes (ArrayDeclar d e) is = do
        VI i <- evalExp e
        when (i < 1) (throwRuntimeError $ NotPositiveArraySize e)
        getIndexes d (i:is)
    getIndexes (ArrayDeclarEnd bt e) is = do
        VI i <- evalExp e
        when (i < 1) (throwRuntimeError $ NotPositiveArraySize e)
        return (bt, i:is)
    alloc (bt, i:is) = do
        values <- forM (genericReplicate i (bt, is)) alloc
        l <- newLoc TO
        assign l (VA $ Array.array (0, i - 1) (zip [0..] values))
        return l
    alloc (bt, []) = newLoc (getType bt)
  in do
    ret <- getIndexes decl []
    alloc ret

assign:: Loc -> Value -> Eval ()
assign l v = do
    (st, ml, stm) <- get
    put (Map.insert l v st, ml, stm)

getVal :: Loc -> Eval Value
getVal l = do
    (st, _, _) <- get
    let Just v = Map.lookup l st
    return v

callFun :: Fun -> [Exp] -> Eval Value
callFun (F args stms env mn t) es = let
    createEnv (((n, ArgVal), e):rest) fenv = do
        l <- newLoc TO
        v <- evalExp e
        assign l v
        createEnv rest (Map.insert n (EVL l) fenv)
    createEnv (((n, ArgRef), e):rest) fenv = do
        l <- getAddr e
        createEnv rest (Map.insert n (EVL l) fenv)
    createEnv [] fenv = return fenv
    addRecursion env' fun mn' =
        case mn' of
            Just n -> Map.insert n (EVF fun) env'
            Nothing -> env'
  in do
    env' <- createEnv (zip args es) env
    let env'' = addRecursion env' (F args stms env mn t) mn
    ret <- local (const env'') (runFun (evalStms stms))
    case ret of
        Left v -> return v
        Right _ -> return $ getDefVal t

evalStms :: [Stm] -> FunEval Value ()
evalStms (stm@(SDec decl):stms) = do
    lift $ putStm stm
    env <- lift $ evalDecl decl
    local (const env) (evalStms stms)

evalStms (stm:stms) = do
    lift $ putStm stm
    evalStm stm
    evalStms stms

evalStms [] = right ()

evalStm :: Stm -> FunEval Value ()
evalStm (stm@(SWhile e stm')) = do
    VB b <- lift $ evalExp e
    when b (evalStms [stm', stm])

evalStm (SIfElse e s1 s2) = do
    VB b <- lift $ evalExp e
    if b then evalStms [s1] else evalStms [s2]

evalStm (SIf e s) = do
    VB b <- lift $ evalExp e
    when b (evalStms [s])

evalStm (SExp e) = void $ lift $ evalExp e

evalStm (SBlock stms) = evalStms stms

evalStm (SReturn e) = do
    v <- lift $ evalExp e
    left v

evalStm (stm@(SDec _)) = evalStms [stm]

evalStm (for@(SFor{})) = let
    evalForStm (stm@(SFor e1 e2 e3 stm')) bi = do
        when bi (void $ lift $ evalExp e1)
        VB b <- lift $ evalExp e2
        when b $ do
             evalStms [stm', SExp e3]
             evalForStm stm False
  in evalForStm for True

evalStm (SPrint e) = do
    v <- lift $ evalExp e
    case v of
        VI i -> liftIO $ print i
        VB b -> liftIO $ print b

getAddr :: Exp -> Eval Loc
getAddr (EVar n) = do
    Just (EVL l) <- asks (Map.lookup n)
    return l

getAddr (EPreDecr e) = do
    l <- getAddr e
    VI i <- getVal l
    assign l (VI (i - 1))
    return l

getAddr (EPreIncr e) = do
    l <- getAddr e
    VI i <- getVal l
    assign l (VI (i + 1))
    return l

getAddr (EAss e1 Assign e2) = do
    l <- getAddr e1
    v <- evalExp e2
    assign l v
    return l

getAddr (EAss e1 op e2) = let
    convOp AssignMul = (*)
    convOp AssignDiv = div
    convOp AssignAdd = (+)
    convOp AssignSub = (-)
  in do
    l <- getAddr e1
    VI i1 <- getVal l
    VI i2 <- evalExp e2
    when (i2 == 0 && op == AssignDiv) (throwRuntimeError $ ZeroDivision e2)
    let v = VI $ convOp op i1 i2
    assign l v
    return l

getAddr (EArray e1 e2) = do
    l <- getAddr e1
    VA a <- getVal l
    VI i <- evalExp e2
    let (x, y) = Array.bounds a
    when (i < x || i > y) (throwRuntimeError $ IndexOutOfBounds e2 i)
    return $ a Array.! i

throwRuntimeError :: ErrorKind -> Eval a
throwRuntimeError err = do
    (_, _, stm) <- get
    throwError $ RE err stm

putStm :: Stm -> Eval ()
putStm stm = do
    (st, ml, _) <- get
    put (st, ml, stm)
