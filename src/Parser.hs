module Parser where

import Lexsyntax(Token)
import Parsyntax(myLexer, pProgram)
import Abssyntax(Program)
import ErrM(Err)

parse :: String -> Err Program
parse s = let ts = myLexer s in pProgram ts
