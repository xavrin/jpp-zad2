#!/bin/bash
cd src
ghc Main
P=src/Main
cd ..
ok=1
for i in good/*.cpm
do
    $P $i > out
    if diff ${i/cpm/out} out
    then
        echo $i " OK"
    else
        echo $i " failed"
        ok=0
    fi
    rm out
done

for i in bad/*.cpm
do
    if ! $P $i > /dev/null
    then
        echo $i " OK"
    else
        echo $i " failed"
        ok=0
    fi
done
if [ $ok == 1 ]
then
    echo "All tests passed"
else
    echo "Some of the tests failed"
fi
